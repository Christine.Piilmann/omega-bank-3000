from django.urls import path
from . import views
from django.views.generic import RedirectView

app_name = 'bank_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('customer/', views.customerPortal, name='customer_portal'),
    path('<int:pk>/', views.accountDetails, name='account_details'),
    path('movement/<int:pk>/', views.movementDetails, name='movement_details'),
    path('create_account/', views.createAccount, name='create_account'),
    path('staff_customer_view/<int:pk>/', views.staffCustomerView, name='staff_customer_view'),
    path('create_customer/', views.createCustomer, name='create_customer'),
    # redirect to customer view when two-factor is complete
    path('account/two_factor/setup/complete/', RedirectView.as_view(url='/customer/')),
    path('make_transfer/', views.make_transfer, name='make_transfer'),
    path('internal_transfer', views.internal_transfer, name='internal_transfer'),
    path('bank_to_bank_transfer', views.bank_to_bank_transfer, name='bank_to_bank_transfer'),
    path('transaction_details/<int:transactionId>/', views.make_transfer, name='transaction_details'),
    path('staff_portal/', views.staffPortal, name='staff_portal'),
    path('staff_search/', views.staffSearch, name='staff_search'),
    path('take_loan/', views.take_loan, name='take_loan'),
    path('approve_loans/', views.approve_loan, name='staff_approve_loans'),
    path('approve/<int:pk>/', views.approve, name='approve'),
    path('decline/<int:pk>/>', views.decline, name='decline'),
]
