from django.contrib import admin
from .models import Account, Loan, Rank, CustomUser, TransactionId, Ledger


admin.site.register(Account)
admin.site.register(Rank)
admin.site.register(CustomUser)
admin.site.register(Loan)
admin.site.register(TransactionId)
admin.site.register(Ledger)
# Register your models here.
